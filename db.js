exports.newConn = function(callback) {
    console.log("++++++++++")

    //db.createUser( { user: "admin1",pwd: "admin1",customData: { employeeId: 12345 },roles: [ { role: "clusterAdmin", db: "admin" },{ role: "readAnyDatabase", db: "admin" },"readWrite"] },{ w: "majority" , wtimeout: 5000 } )
    //db.createUser({user: "project",pwd: "password",roles:[{ role: "readWrite", db: "config" },"clusterAdmin"]})
    //MongoClient.connect("mongodb://"+DB_CONFIG.DB_USER+":"+DB_CONFIG.DB_PWD+"@"+DB_CONFIG.DB_HOST+":27017/"+DB_CONFIG.DB_NAME, function(err, db) {
    MongoClient.connect('mongodb://project:project@192.168.102.231:27017/project', function(err, db) {

        global.db.CoverLetter = db.collection('CoverLetter')
        global.db.schoolUserData = db.collection('school_user_data')
        global.db.subscribeVacancy = db.collection('subscribeVacancy')
        global.db.CompanyProgects = db.collection('Company-progects')
        global.db.files = db.collection('files')
        global.db.recommendation= db.collection('recommendation')
        global.db.workExp = db.collection('work-exp')
        global.db.users = db.collection('users')
        global.db.dialogs = db.collection('dialogs')
        global.db.schools = db.collection('schools')
        global.db.resume = db.collection('resume')
        global.db.vacancy = db.collection('vacancy')
        global.db.subaccount = db.collection('subaccount')
        global.db.locations = db.collection('locations')
        global.db.savedSearches = db.collection('savedSearches')
        global.db.messages = db.collection('messages')
        global.db.options = db.collection('options')
        global.db.bookmarks = db.collection('bookmarks')
        global.db.options.find({}).toArray(function(err, data) {
            if (err) {
                console.log('[ALERT] prototype.GeUserInfo pcDB.users')
                console.error(err);
                callback(err,null)
            } else {

              //  global.cache.options = Object.assign({},data[0],data[1])

                callback(null, 'Module Database: [OK]');
            }
        })

    });
};
